﻿namespace CommandLineHelpers
{
    /// <summary>
    /// Represents the output of running a command line process
    /// </summary>
    public interface ICommandLineResult
    {

        /// <summary>
        /// The text output by the process
        /// </summary>
        string Output { get; set; }

        /// <summary>
        /// Exit code of the process when it finished
        /// </summary>
        int ExitCode { get; set; }

        /// <summary>
        /// Throws an exception if the process did not return with one of these exit codes
        /// </summary>
        void ExpectExitCode(params int[] validExitCodes);
    }
}