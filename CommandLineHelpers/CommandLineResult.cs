﻿using System;
using System.Linq;

namespace CommandLineHelpers
{

    /// <inheritdoc />
    public class CommandLineResult : ICommandLineResult
    {

        /// <inheritdoc />
        public string Output { get; set; }

        /// <inheritdoc />
        public int ExitCode { get; set; }

        public CommandLineResult(int exitCode)
            : this(null, exitCode)
        {

        }

        public CommandLineResult(string output)
            : this(output, 0)
        {

        }

        public CommandLineResult(string output, int exitCode)
        {
            Output = output;
            ExitCode = exitCode;
        }

        /// <inheritdoc />
        public void ExpectExitCode(params int[] validExitCodes)
        {
            if (!validExitCodes.Contains(ExitCode))
                throw new InvalidOperationException(
                    string.Format(
                    "Expected one of the exit codes '{0}' but instead got '{1}'\r\nOutput:\r\n{2}",
                    String.Join(", ", validExitCodes),
                    ExitCode,
                    Output));
        }

    }
}
