namespace CommandLineHelpers
{
    /// <summary>
    /// Represents something which can run a command line process and return the result
    /// </summary>
    public interface ICommandLineRunner
    {
        /// <summary>
        /// RUns a process and returns the result
        /// </summary>
        /// <param name="workingDirectory">The working directory which should be set when the process starts</param>
        /// <param name="executable">The process to run</param>
        /// <param name="arguments">Arguments to pass to the process</param>
        /// <param name="timeout">How long to wait for a result</param>
        /// <returns></returns>
        ICommandLineResult Run(string workingDirectory, string executable, string arguments, int timeout = 1000 * 120);
    }
}