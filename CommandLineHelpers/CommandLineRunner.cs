using System;
using System.Diagnostics;
using System.Text;

namespace CommandLineHelpers
{
    /// <inheritdoc />
    public class CommandLineRunner : ICommandLineRunner
    {

        /// <inheritdoc />
        public ICommandLineResult Run(string workingDirectory, string executable, string arguments, int timeout = 1000 * 120)
        {
            if (workingDirectory == null) throw new ArgumentNullException("workingDirectory");
            if (executable == null) throw new ArgumentNullException("executable");
            if (arguments == null) throw new ArgumentNullException("arguments");

            var startInfo = new ProcessStartInfo(executable, arguments);
            startInfo.WorkingDirectory = workingDirectory;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;

            var process = new Process();

            var output = new StringBuilder();

            process.StartInfo = startInfo;

            process.Start();
            process.WaitForExit(timeout);

            string line;
            while ((line = process.StandardOutput.ReadLine()) != null)
                output.AppendLine(line);
            while ((line = process.StandardError.ReadLine()) != null)
                output.AppendLine(line);

            return new CommandLineResult(output.ToString(), process.ExitCode);
        }

    }
}